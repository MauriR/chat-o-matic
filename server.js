const { GraphQLServer } = require('graphql-yoga')

const messages = [] //para los resolvers (ver más adelante)


//todos los servers graphql necesitan types, es un patrón
// por lo tanto vamos a definir cómo son nuestro mensajes
// para ello escribimos type Message y entre curlys escribimos los campos que hay dentro del mensaje
//Las exclamaciones significan que el campo es requerido. 

const typeDefs = `
type Message { 
  id: ID!
  user: String!
  content: String! 

}

type Query {
 messages: [Message!]
}
`

// Con typeDefs hemos visto cómo se crean y cómo sonn los mensajes. 
//Para hacerles query  escribiremos type query


//resolvers sería como decir: 'Toma tus types. Ahora cómo consigo la información
//y aquí entran los resolvers que hacen algó así cómo unir las keys que en en las definiciones de
//los types. En este caso un query y los mensajes

const resolvers = {
  Query: {
    messages: () => messages,
  }
}



const server = new GraphQLServer({ typeDefs, resolvers })
server.start(({ port }) => {
  console.log(`Esa Peña! en el puerto ${port}`)
})